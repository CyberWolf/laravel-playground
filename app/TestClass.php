<?php


namespace App;

use App\Models\PhoneNumber;
use App\Services\UserService;

$service = new UserService();

echo 'User info:' . PHP_EOL;
$service->userInfo(1);

$randomPhone = PhoneNumber::inRandomOrder()->first();
$service->addFunds($randomPhone->number);

$service->createUser('Dark Lord', '01-12-1578');

$service->addPhoneNumberToUser(1, '380501234567');

// Destroy user. Foreign keys clear related data
$service->destroyUser(1);
