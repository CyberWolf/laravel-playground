<?php

namespace App\Http\Controllers;

use App\Models\Operator;
use App\Models\PhoneNumber;
use App\Models\User;
use App\Services\UserService;

class TestController extends Controller
{
    /**
     * @param  UserService  $service
     * @return false
     */
    public function test(UserService $service)
    {
        $number = PhoneNumber::inRandomOrder()->first();

        $service->addFunds($number->number, 100);
        $service->addFunds($number->number, 200);

        $user = $service->createUser('User Name', '12-12-1996');

        $user = User::factory()->create();
//        $service->addPhoneNumberToUser($user->id, 380501234);
        $service->addPhoneNumberToUser($user->id, '380501234568');
        $service->addPhoneNumberToUser($user->id, '380631234568');
        $service->addPhoneNumberToUser($user->id, '380681234568');
        $user->refresh();

        $service->destroyUser($user->id);

        print 'Look inside App\Http\Controllers\TestController';
    }
}
