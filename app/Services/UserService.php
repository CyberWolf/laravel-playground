<?php


namespace App\Services;


use App\Models\Operator;
use App\Models\PhoneNumber;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use stdClass;
use Symfony\Component\String\Exception\InvalidArgumentException as StringException;

class UserService
{
    private function checkUniqueness($operatorId, $phoneNumber)
    {
        $phonesFromDb = PhoneNumber::where([
            ['operator_id', $operatorId],
            ['phone_number', $phoneNumber]
        ])->count();

        if ($phonesFromDb != 0) {
            throw new \Exception('Phone not unique');
        }
    }

    private function extractAndValidateDataFromNumber($phoneNumber)
    {
        $code = substr($phoneNumber, 3,2);

        $operator = Operator::where('code', $code)->firstOrFail();

        $phoneNumber = substr($phoneNumber, 5,7);

        return [$operator->id, $phoneNumber];
    }

    private function findPhoneNumberEntity($phoneNumber)
    {
        list($operatorId, $phoneNumber) = $this->extractAndValidateDataFromNumber($phoneNumber);

        return PhoneNumber::where('operator_id', $operatorId)
            ->where('phone_number', $phoneNumber)
            ->firstOrFail();
    }

    protected function validateInputtedNumber($phoneNumber)
    {
        if (!preg_match('/^(380)+([0-9]{9}$)/m', $phoneNumber)) {
            throw new StringException('Wrong format. Required 380123456789');
        }
    }


    public function userInfo(int $userId)
    {
        // Find user or stop execution.
        // with() eager loads relation, that defined in App\Models\User
        $user = User::with('phoneNumbers')->findOrFail($userId);

        return $user->toArray();
    }

    public function addFunds($phoneNumber, $value)
    {
        $this->validateInputtedNumber($phoneNumber);
        $phoneEntity = $this->findPhoneNumberEntity($phoneNumber);

        //Through setter in PhoneNumber::class
        $phoneEntity->balance = $value;
        $phoneEntity->save();

        return $phoneEntity;
    }

    public function createUser(string $name, string $birth_date)
    {
        $user = User::create([
            'name' => $name,
            'birth_date' => Carbon::parse($birth_date)
        ]);

        return $user;
    }

    // Pretends that phone number is valid.
    public function addPhoneNumberToUser(int $userId, string $phoneNumber): string|PhoneNumber
    {
        $this->validateInputtedNumber($phoneNumber);
        list($operatorId, $phoneNumber) = $this->extractAndValidateDataFromNumber($phoneNumber);
        $this->checkUniqueness($operatorId, $phoneNumber);

        $user = User::findOrFail($userId);
        $phoneNumber = $user->phoneNumbers()->create([
            'user_id' => $user->id,
            'operator_id' => $operatorId,
            'phone_number' => $phoneNumber
        ]);

        return $phoneNumber;
    }

    public function destroyUser(int $id): bool
    {
        return User::destroy($id);
    }
}
