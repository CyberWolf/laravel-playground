<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model
{
    use HasFactory;

    protected const COUNTRY_CODE = 380;

    // Defines which relations preload by default
    protected $with = ['operator'];

    protected $fillable = [
        'user_id',
        'operator_id',
        'phone_number'
    ];

    // Additional fields, that may not be present in database
    protected $appends = [
        'number'
    ];

    protected $guarded = [
        'balance'
    ];

    // Define relation on operator Model
    public function owner()
    {
        //Possible set additional params in hasOne() method
        //if related table or column have names different from Laravel convention
        return $this->belongsTo(User::class);
    }

    // Define relation on operator Model
    public function operator()
    {
        //Possible set additional params in hasOne() method
        //if related table or column have names different from Laravel convention
        return $this->belongsTo(Operator::class);
    }

    // Laravel's setter alternative
    public function setBalanceAttribute(int|float $value)
    {
        if (0 < $value && $value <= 100) {
            $this->attributes['balance'] += $value;
        } else {
            throw new Exception('Balance change value accept values between 0 and 100');
        }
    }

    public function setProtectedBalanceAttribute(int|float $value)
    {
        // Absolute balance setter. In real project need add check by Admin role or something else
        $this->attributes['balance'] = $value;
    }

    // Laravel's getter alternative
    public function setPhoneNumberAttribute($value)
    {
        if (is_string($value)){
            $this->attributes['phone_number'] = $value;
        }

        if (is_int($value)){
            $this->attributes['phone_number'] = sprintf('%07d', $value);
        }

    }

    // Laravel's getter alternative
    public function getNumberAttribute()
    {
        return self::COUNTRY_CODE . $this->operator->code . $this->phone_number;
    }
}
