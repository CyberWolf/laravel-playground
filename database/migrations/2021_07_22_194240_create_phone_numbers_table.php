<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhoneNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_numbers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('operator_id');
            $table->string('phone_number', 7);
            $table->float('balance', 7, 2)->default(0);
            $table->timestamps();

            $table->unique(['operator_id', 'phone_number']);

            $table
                ->foreign('user_id')
                ->on('users')
                ->references('id')
                ->onDelete('cascade');

            $table
                ->foreign('operator_id')
                ->on('operators')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone_numbers');
    }
}
