<?php

namespace Database\Factories;

use App\Models\Operator;
use App\Models\PhoneNumber;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

class PhoneNumberFactory extends Factory
{
    protected $operatrors;

    //Overridden constructor to defied bug with faker count and generate non repeatable per user operators
    public function __construct(
        $count = null,
        ?Collection $states = null,
        ?Collection $has = null,
        ?Collection $for = null,
        ?Collection $afterMaking = null,
        ?Collection $afterCreating = null,
        $connection = null
    ) {
        parent::__construct($count, $states, $has, $for, $afterMaking, $afterCreating, $connection);
        $this->operatrors = Operator::inRandomOrder()->pluck('id')->toArray();
    }

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PhoneNumber::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'phone_number' => sprintf('%07d', rand(1,9999999)),
            'protected_balance' => rand(-50, 150)
        ];
    }

    public function randomOperator()
    {
        return $this->state(function (array $attributes) {
            return [
                'operator_id' => array_pop($this->operatrors),
            ];
        });
    }
}
