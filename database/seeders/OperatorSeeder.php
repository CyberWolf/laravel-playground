<?php

namespace Database\Seeders;

use App\Models\Operator;
use Illuminate\Database\Seeder;

class OperatorSeeder extends Seeder
{
    private $operators;

    public function __construct()
    {
        $this->operators = config('app.defaultOperators');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->operators as $operator) {
            Operator::factory()->create(['code' => $operator]);
        }
    }
}
