<?php

namespace Database\Seeders;

use App\Models\Operator;
use App\Models\PhoneNumber;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    private $operators;

    public function __construct()
    {
        $this->operators = config('app.defaultOperators');
    }

    private function selectRandomUniqueOperators()
    {
        return $this->operators[array_rand($this->operators)];
//        shuffle($operators);
////        return array_pad($operators, $quantity,0);
//        return array_pop($operators);

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->operators = Operator::pluck('id')->toArray();

        User::factory()
            ->count(2000)
            ->create()
            ->each(function ($user) {
                $quantity = rand(1,3);

                $user
                    ->phoneNumbers()
                    ->saveMany( // Bulk save
                        PhoneNumber::factory()
                            ->times($quantity)
                            ->randomOperator() // State defined in PhoneNumberFactory
                            ->make() // Create just class objects without saving into database
                );
            });
    }
}
