## How to launch

1. If no local composer or PHP. Download min PHP container and install dependencies.
    ```bash
    docker run --rm \
        -u "$(id -u):$(id -g)" \
        -v $(pwd):/opt \
        -w /opt \
        laravelsail/php80-composer:latest \
        composer install --dev --ignore-platform-reqs
    ```
1. Run built in Laravel Docker container
    ```bash
    vendor/bin/sail up
    ```

1. Copy **.env.example** to **.env**

1. Seed database by running one of next commands depending on yours environment
   from root path or import SQL dump.
    `sail artisan migrate --seed`
    `php artisan migrate --seed`
   
    **MySQL Dump in root path "dump.sql"**
## Ключевые файлы файлы

1. готовая БД: **./dump.sql**;
1. скрипт, в котором реализован класс **./app/Services/UserService.php**;
1. скрипт, в котором мы этот класс подключаем и вызываем его методы (в нем будем все проверять)
    -  **./app/TestClass.php** 
    - или **./app/Http/Controllers/TestController.php** - Get request to localhost 
1. SQL запросы, в отдельном файле (проверяем через Navicat, sqlyog);
    **./README.md**

## MySQL Queries

1. Баланс по каждому пользователю (сумма денег по всем номерам и операторам каждого пользователя)
    ```mysql
    SELECT pn.user_id, SUM(pn.balance) FROM users
        left join phone_numbers pn on users.id = pn.user_id
        GROUP BY pn.user_id;   
   ``` 
   ```mysql
    SELECT user_id, SUM(balance) as summary_balance
        FROM phone_numbers
        GROUP BY user_id;
    ```
1. количество номеров телефонов по операторам (список: код оператора, кол-во номеров этого оператора);
    ```mysql
    SELECT o.code, count(phone_numbers.id)
        FROM phone_numbers
        left JOIN
        operators o on o.id = phone_numbers.operator_id
        GROUP BY operator_id;
    ```
1. Количество телефонов у каждого пользователя (списоки: мя пользователя, кол-во номеров у пользователя);
    ```mysql
        SELECT u.name, COUNT(pn.user_id)
            FROM users as u
            left join phone_numbers pn on u.id = pn.user_id
            GROUP BY u.id
    ```
1. Вывести имена 10 пользователей с максимальным балансом на счету (максимальный баланс по одному номеру);
    ```mysql
    SELECT u.name
        FROM phone_numbers as pn
        left join users u on u.id = pn.user_id
        GROUP BY user_id
        order by MAX(pn.balance) DESC
        LIMIT 10;
    ```
   
`exec('mysqldump --user=sail --password=password --host=mysql playground > ./dump.sql');`
